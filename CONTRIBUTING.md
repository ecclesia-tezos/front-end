# CONTRIBUTING

## Deployment
Currently the deployment process is not automated, but is automatable.
To deploy the application, you need to:

1. Run the prod build script
```
bin/compile.sh
```
2. Upload the content to the AWS S3 bucket of choice.
3. Invalidate the cloudfront distribution to make sure clients are getting the newest version.
