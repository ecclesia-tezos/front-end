#!/bin/bash
docker kill react-dev-server
docker run --rm --name react-dev-server -d -p 3000:3000 -v "$(pwd)":/app front-end-dev-env yarn start
