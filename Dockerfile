FROM node:13.8.0

COPY . /app

WORKDIR /app

RUN apt-get update && apt-get install -y libusb-1.0-0-dev libudev-dev

RUN yarn install

EXPOSE 3000
