import React from 'react';
import moment from 'moment';

import '../styles/timer.css';

export default class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      wording: '',
      actionUsed: false,
      days: 0,
      hours: 0,
      minutes: 0,
      seconds: 0
    }

    this.paddedTime = this.paddedTime.bind(this);
  }

  async componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  tick() {
    let currentMoment = moment();

    if (currentMoment.isBefore(this.props.phaseStart)) {
      let diff = moment(this.props.phaseStart).diff(currentMoment);
      let duration = moment.duration(diff);
      this.setState({
        days: duration.days(),
        hours: duration.hours(),
        minutes: duration.minutes(),
        seconds: duration.seconds(),
      });
    } else if (this.actionIsAvailable() && !this.state.actionUsed) {
      clearInterval(this.interval);
      this.setState({ actionUsed: true });
      this.props.action();
    } else {
      clearInterval(this.interval);
    }
  }

  actionIsAvailable() {
    let currentMoment = moment();
    let interactableEnd = moment(this.props.phaseEnd);
    return !this.props.phaseEnd || currentMoment.isBefore(interactableEnd);
  }

  paddedTime(time) {
    return time.toString().padStart(2, '0');
  }

  render() {
    let currentMoment = moment();
    let message = '';
    let messageColorClass = '';
    let timerColorClass = '';
    if (currentMoment.isBefore(this.props.phaseStart)) {
      messageColorClass = 'timer__message--before-start';
      timerColorClass = 'timer--before-start';
      message = this.props.waitWording;
    } else if (this.actionIsAvailable()) {
      messageColorClass = 'timer__message--before-end';
      timerColorClass = 'timer--before-end';
      message = this.props.proceedWording;
    } else {
      messageColorClass = 'timer__message--after-end';
      timerColorClass = 'timer--after-end';
      message = 'You are too late for this election, sorry!';
    }

    return (
      <div className={ `timer ${timerColorClass}` }>
        <div className={ `timer__message ${messageColorClass}` }>
        { message }
        </div>
        <div className="timer__widget">
          <div className="timer__widget-number">
          { this.paddedTime(this.state.days) }
          </div>
          <div className="timer__widget-separator"> : </div>
          <div className="timer__widget-number">
          { this.paddedTime(this.state.hours) }
          </div>
          <div className="timer__widget-separator"> : </div>
          <div className="timer__widget-number">
          { this.paddedTime(this.state.minutes) }
          </div>
          <div className="timer__widget-separator"> : </div>
          <div className="timer__widget-number">
          { this.paddedTime(this.state.seconds) }
          </div>
        </div>
      </div>
    );
  }
}
