import React from 'react';

import ContractInspector from '../voting/contract_inspector';
import CryptoServiceAdapter from '../crypto/crypto_service_adapter';

import Timer from './timer';
import LoadingSpinner from './loading_spinner';

export default class Tallier extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contractInspector: new ContractInspector(),
      cryptoServiceAdapter: new CryptoServiceAdapter(),
      isOpen: false,
      tallyComplete: false
    }

    this.setIsOpen = this.setIsOpen.bind(this);
    this.formatTally = this.formatTally.bind(this);
  }

  async componentDidUpdate() {
    if (this.state.isOpen && !this.state.tallyComplete) {
      await this.tally();
    }
  }

  setIsOpen() {
    this.setState({ isOpen: true });
  }

  async tally() {
    if (this.state.isOpen) {
      let credentials = await this.state.contractInspector.getCredentials(
        this.props.contractAddress
      );

      let votes = await this.state.contractInspector.getVotes(
        this.props.contractAddress
      );

      let verifiedBallots = await this.state.cryptoServiceAdapter.verify(
        votes,
        credentials,
        this.props.modulus
      );

      const validBallots = verifiedBallots.filter(ballot => ballot.is_valid);
      let tally = {};

      for (let candidate of validBallots.map(ballot => ballot.candidate)) {
        tally[candidate] = tally[candidate] ? tally[candidate] + 1 : 1;
      }

      this.setState({ tally: tally, tallyComplete: true });
    }
  }

  formatTally(tally) {
    const votesIndex = 1;
    console.log(tally);
    let sortedTally = Object.entries(tally).sort((leftCandidate, rightCandidate) => {
      return rightCandidate[votesIndex] - leftCandidate[votesIndex];
    });

    let result = [];
    for (let [candidate, votes] of sortedTally) {
      result.push(<li key={ candidate }> { candidate } with { votes } votes</li>);
    }

    return <ol> { result } </ol>;
  }

  render() {
    let phaseOpenedState = this.state.tallyComplete ? this.formatTally(this.state.tally): <LoadingSpinner/>;
    let phaseClosedState = <Timer
        waitWording="Please wait while the votes are being counted in a decentralized way"
        proceedWording=""
        phaseStart={ this.props.phaseStart }
        action={ this.setIsOpen }
      />;
    return (
      <div>
        { this.state.isOpen ? phaseOpenedState : phaseClosedState }
      </div>
    );
  }
}
