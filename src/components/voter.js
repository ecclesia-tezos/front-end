import React from 'react';

import TransactionStatusUpdater from '../voting/transaction_status_updater';
import ConseilServiceAdapter from '../voting/conseil_service_adapter';
import CryptoServiceAdapter from '../crypto/crypto_service_adapter';
import ContractInspector from '../voting/contract_inspector';

import Timer from './timer';
import LoadingSpinner from './loading_spinner';

import '../styles/voter.css';

export default class Voter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cryptoServiceAdapter: new CryptoServiceAdapter(this.props.email, this.props.token),
      conseilServiceAdapter: new ConseilServiceAdapter(),
      transactionStatusUpdater: new TransactionStatusUpdater(),
      contractInspector: new ContractInspector(),
      transactionStatus: 'unsent'
    }

    this.handleCandidateChange = this.handleCandidateChange.bind(this);
    this.vote = this.vote.bind(this);
    this.renderPhase = this.renderPhase.bind(this);
  }

  handleCandidateChange(event) {
    this.setState({ candidate: event.target.value });
  }

  async vote() {
    if (this.state.transactionStatus === 'pending') {
      return;
    }

    this.setState({ transactionStatus: 'pending' });

    const otherCredentials = await this.getOtherCredentials();

    let vote = await this.state.cryptoServiceAdapter.vote(
      this.props.candidate,
      this.props.modulus,
      this.props.credentials.public_value,
      otherCredentials,
      this.props.credentials.serial_number,
      this.props.credentials.randomness
    );

    this.props.setVoteKey(vote.key);

    let statuses = await this.splitInMultipleTransactions(vote.vote, this.props.contractAddress, 3);
    this.setState({ transactionStatus: statuses.join(' ') });
    if (statuses.every(status => status === 'applied')) {
      this.props.advancePhase();
    }
  }

  async splitInMultipleTransactions(voteCommitment, contractAddress, transactionNum) {
    let txStatuses = [];
    let pieceSize = Math.floor(voteCommitment.length / transactionNum);
    for (let i = 0; i < transactionNum; i++) {
      let commitmentPiece = voteCommitment.substring(i * pieceSize, (i + 1) * pieceSize);
      if (i + 1 === transactionNum) {
        commitmentPiece = voteCommitment.substring(i * pieceSize);
      }

      let txResult = await this.state.conseilServiceAdapter.vote(
        commitmentPiece, i + 1, this.props.token, contractAddress
      );
      let txStatus = await this.state.transactionStatusUpdater.updateTransactionStatus(txResult);
      txStatuses.push(txStatus);
    }

    return txStatuses;
  }

  async getOtherCredentials() {
    let credentials = await this.state.contractInspector.getCredentials(
      this.props.contractAddress
    );

    // make sure to exclude our own credential from this list
    const index = credentials.indexOf(this.props.credentials.public_value);
    if (index > -1) {
      credentials.splice(index, 1);
    }
    return credentials;
  }

  renderPhase() {
    return (
      <div className="voter">
        <Timer
          waitWording="Your key is being processed. Please wait"
          proceedWording="Please vote for your candidate"
          phaseStart={ this.props.phaseStart }
          phaseEnd={ this.props.phaseEnd }
          action={ this.vote }
        />
      </div>
    );
  }

  render() {
    return this.state.transactionStatus !== 'pending' ? this.renderPhase() : <LoadingSpinner/>;
  }
}
