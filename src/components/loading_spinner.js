import React from 'react';

import '../styles/loading_spinner.css';

export default class LoadingSpinner extends React.Component {

  render() {
    let bars = [...Array(12).keys()].map(key => <div key={ key.toString() }></div>);

    return (
      <div className="loading-spinner">
        <div className="loading-spinner__widget">
        { bars }
        </div>
        <div className="loading-spinner__message">
        { 'Please wait and do not reload. This could take a couple of minutes.' }
        </div>
      </div>
    );
  }
}

