import React from 'react';

import TransactionStatusUpdater from '../voting/transaction_status_updater';
import CryptoServiceAdapter from '../crypto/crypto_service_adapter';
import ConseilServiceAdapter from '../voting/conseil_service_adapter';

import Timer from './timer';
import LoadingSpinner from './loading_spinner';

import '../styles/credential_generator.css';

export default class CredentialGenerator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cryptoServiceAdapter: new CryptoServiceAdapter(this.props.email, this.props.token),
      conseilServiceAdapter: new ConseilServiceAdapter(),
      transactionStatusUpdater: new TransactionStatusUpdater(),
      transactionStatus: 'unsent'
    }

    this.generateCredentials = this.generateCredentials.bind(this);
  }

  async generateCredentials() {
    if (this.state.transactionStatus === 'pending') {
      return;
    }

    this.setState({ transactionStatus: 'pending' });

    let credentials = await this.state.cryptoServiceAdapter.mintCredentials(
      this.props.modulus
    );

    this.props.setCredentials(credentials);

    let transactionResult = await this.state.conseilServiceAdapter.addCredential(
      credentials.public_value, this.props.token, this.props.contractAddress
    );

    console.log(transactionResult);
    let transactionStatus = await this.state.transactionStatusUpdater.updateTransactionStatus(
      transactionResult
    );

    this.setState({ transactionStatus: transactionStatus });

    if (transactionStatus === 'applied') {
      this.props.advancePhase();
    }
  }

  renderPhase() {
    return(
      <div className="credential-generator">
        <Timer
          waitWording="Get your key ready. It was sent to you by lena@electis.io"
          proceedWording="Input the key provided to you"
          phaseActionableInterval={ this.props.phaseActionableInterval }
          phaseStart={ this.props.phaseStart }
          phaseEnd={ this.props.phaseEnd }
          action={ this.generateCredentials }
        />
        <div className="credential-generator__message">
        </div>
        <div>
          <div>
            <input
              type="radio"
              name="candidate"
              value="Yes, but only if one wants to join a stream that produces output (i.e. if they're not here for only education)"
              checked={ this.props.candidate === "Yes, but only if one wants to join a stream that produces output (i.e. if they're not here for only education)" }
              onChange={ this.props.onCandidateChange }
            />
            <label>Yes, but only if one wants to join a stream that produces output (i.e. if they're not here for only education)</label>
          </div>
          <div>
            <input
              type="radio"
              name="candidate"
              value="Yes, but only if one wants to run for a position (e.g. VP of Legal)"
              checked={ this.props.candidate === "Yes, but only if one wants to run for a position (e.g. VP of Legal)" }
              onChange={ this.props.onCandidateChange }
            />
            <label>Yes, but only if one wants to run for a position (e.g. VP of Legal)</label>
          </div>
          <div>
            <input
              type="radio"
              name="candidate"
              value="Yes, but for different reasons than above"
              checked={ this.props.candidate === "Yes, but for different reasons than above" }
              onChange={ this.props.onCandidateChange }
            />
            <label>Yes, but for different reasons than above</label>
          </div>
          <div>
            <input
              type="radio"
              name="candidate"
              value="No"
              checked={ this.props.candidate === "No" }
              onChange={ this.props.onCandidateChange }
            />
            <label>No</label>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return this.state.transactionStatus !== 'pending' ? this.renderPhase() : <LoadingSpinner/>
  }
}
