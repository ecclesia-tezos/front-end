import React from 'react';

import TransactionStatusUpdater from '../voting/transaction_status_updater';
import ConseilServiceAdapter from '../voting/conseil_service_adapter';

import Timer from './timer';
import LoadingSpinner from './loading_spinner';

import '../styles/vote_opener.css';

export default class VoteOpener extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      conseilServiceAdapter: new ConseilServiceAdapter(),
      transactionStatusUpdater: new TransactionStatusUpdater(),
      transactionStatus: 'unsent'
    }

    this.openVote = this.openVote.bind(this);
  }

  async openVote() {
    if (this.state.transactionStatus === 'pending') {
      return;
    }

    this.setState({ transactionStatus: 'pending' });

    let transactionResult = await this.state.conseilServiceAdapter.openVote(
      this.props.voteKey, this.props.token, this.props.contractAddress
    );

    let transactionStatus = await this.state.transactionStatusUpdater.updateTransactionStatus(
      transactionResult
    );

    this.setState({ transactionStatus: transactionStatus });

    if (transactionStatus === 'applied') {
      this.props.advancePhase();
    }
  }

  renderPhase() {
    return (
      <div className="vote-opener">
        <Timer
          waitWording="Please wait until all votes are submitted"
          proceedWording="Please add your vote to the tally"
          phaseStart={ this.props.phaseStart }
          phaseEnd={ this.props.phaseEnd }
          action={ this.openVote }
        />
      </div>
    );
  }

  render() {
    return this.state.transactionStatus !== 'pending' ? this.renderPhase() : <LoadingSpinner/>;
  }
}
