import React from 'react';
import moment from 'moment';
import ContractInspector from './voting/contract_inspector';
import './ballot_box.css';

import CredentialGenerator from './components/credential_generator';
import Voter from './components/voter';
import VoteOpener from './components/vote_opener';
import Tallier from './components/tallier';
import LoadingSpinner from './components/loading_spinner';


export default class BallotBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contractAddress: process.env.REACT_APP_TEZOS_CONTRACT_ADDRESS,
      loaded: false,
      loggedIn: false,
      credentials: {},
      keyStore: {},
      timestamps: [],
      modulus: '',
      phaseIndex: 0,
      actionEnabled: false,
      candidate: '',
      contractInspector: new ContractInspector(),
      email: '',
      token: ''
    }

    this.onKeyStoreDone = this.onKeyStoreDone.bind(this);
    this.onCredentialsChanged = this.onCredentialsChanged.bind(this);
    this.onVoteKeyComputed = this.onVoteKeyComputed.bind(this);
    this.advancePhase = this.advancePhase.bind(this);
    this.handleCandidateChange = this.handleCandidateChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleTokenChange = this.handleTokenChange.bind(this);
    this.logIn = this.logIn.bind(this);
  }

  logIn(event) {
    this.setState({ loggedIn: true });
  }

  handleCandidateChange(event) {
    this.setState({ candidate: event.target.value });
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }

  handleTokenChange(event) {
    this.setState({ token: event.target.value });
  }

  async componentDidMount() {
    let initialState = await this.state.contractInspector.getInitialState(
      this.state.contractAddress
    );
    let timestamps = await this.state.contractInspector.getTimestamps(this.state.contractAddress);

    this.setState({ modulus: initialState.modulus, timestamps: timestamps , loaded: true});

    if (this.shouldShowTally(timestamps)) {
      this.setState({ phaseIndex: 3 });
    }
  }

  shouldShowTally(timestamps) {
    return moment().isAfter(moment(timestamps[1]));
  }

  advancePhase() {
    this.setState({ phaseIndex: this.state.phaseIndex + 1 });
  }

  onKeyStoreDone(keyStore) {
    this.setState({ keyStore: keyStore });
  }

  onVoteKeyComputed(voteKey) {
    this.setState({ voteKey: voteKey });
  }

  onCredentialsChanged(credentials) {
    this.setState({ credentials: credentials });
  }

  render() {
    let phases = [
      <CredentialGenerator
        key="credential_generator"
        keyStore={ this.state.keyStore }
        contractAddress={ this.state.contractAddress }
        modulus={ this.state.modulus }
        onCandidateChange={ this.handleCandidateChange }
        candidate={ this.state.candidate }
        setCredentials={ this.onCredentialsChanged }
        setKeyStore={ this.onKeyStoreDone }
        advancePhase={ this.advancePhase }
        phaseStart={ this.state.timestamps[0] }
        phaseEnd={ this.state.timestamps[1] }
        email={ this.state.email }
        token={ this.state.token }
      />,
      <Voter
        key="voter"
        keyStore={ this.state.keyStore }
        contractAddress={ this.state.contractAddress }
        candidate={ this.state.candidate }
        modulus={ this.state.modulus }
        credentials={ this.state.credentials }
        setVoteKey={ this.onVoteKeyComputed }
        advancePhase={ this.advancePhase }
        actionEnabled={ this.state.actionEnabled }
        phaseStart={ this.state.timestamps[1] }
        phaseEnd={ this.state.timestamps[2] }
        email={ this.state.email }
        token={ this.state.token }
      />,
      <VoteOpener
        key="vote_opener"
        keyStore={ this.state.keyStore }
        contractAddress={ this.state.contractAddress }
        voteKey={ this.state.voteKey }
        advancePhase={ this.advancePhase }
        actionEnabled={ this.state.actionEnabled }
        phaseStart={ this.state.timestamps[2] }
        phaseEnd={ this.state.timestamps[3] }
        email={ this.state.email }
        token={ this.state.token }
      />,
      <Tallier
        key="tallier"
        modulus={ this.state.modulus }
        contractAddress={ this.state.contractAddress }
        phaseStart={ this.state.timestamps[3] }
      />
    ];
    let login = <div>
      <div>
        email:
        <textarea rows="1" cols="30" value={ this.state.email } onChange={ this.handleEmailChange } />
      </div>
      <div>
        token:
        <textarea rows="1" cols="30" value={ this.state.token } onChange={ this.handleTokenChange } />
      </div>
      <button onClick={ this.logIn }>Log in</button>
    </div>;
    let currentPane = <div className="ballot-box__form"> { phases[this.state.phaseIndex] } </div>;
    return (
      <div className="ballot-box">
        { !this.state.loggedIn ? login : this.state.loaded && this.state.loggedIn ? currentPane : <LoadingSpinner/> }
        <div className="ballot-box__disclaimer">
        { 'This is a work in progress implementation and is meant to be used solely for testing purposes.' }
        </div>
      </div>
    );
  }
}
