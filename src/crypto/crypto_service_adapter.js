import axios from 'axios';

export default class CryptoServiceAdapter {

  constructor(email, token) {
    this.axios = axios.create({
      baseURL: process.env.REACT_APP_CRYPTO_SERVICE_URL,
      timeout: 0
    });

    this.auth = { email, token };
  }

  async mintCredentials(modulus) {
    let response = await this.authenticatedPost('/mint', { modulus: modulus });
    return response.data;
  }

  async vote(candidate, modulus, credential, otherCredentials, serialNumber, randomness) {
    let response = await this.authenticatedPost('/vote', {
      modulus: modulus,
      credential: credential,
      candidate: candidate,
      credentials: otherCredentials,
      serial_number: serialNumber,
      randomness: randomness
    });

    return response.data;
  }

  async verify(ballots, credentials, modulus) {
    let response = await this.axios.post('/verify', {
      modulus: modulus,
      ballots: ballots,
      credentials: credentials
    });

    return response.data.verified_ballots;
  }

  authenticatedPost(endpoint, data) {
    const payload = Object.assign({}, this.auth, data);
    return this.axios.post(endpoint, payload);
  }
}
