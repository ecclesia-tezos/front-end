import { TezosConseilClient } from 'conseiljs';

const CONSEIL_SERVER = {
  "url": 'https://conseil-dev.cryptonomic-infra.tech:443',
  "apiKey":"1dd01906-f39c-4010-b673-3daac6e4a536",
  "network":"carthagenet"
};

export default class UpdateTransactionStatus {

  async updateTransactionStatus(transactionResult) {
    const groupId = this.cleanHash(transactionResult.operationGroupID);

    const confirmationResult = await TezosConseilClient.awaitOperationConfirmation(
      CONSEIL_SERVER,
      CONSEIL_SERVER.network,
      groupId,
      20,
      31
    );
    return confirmationResult[0].status;
  }

  cleanHash(hash) {
    return hash.replace(/"/g, '').replace(/\n/, '');
  }
}



