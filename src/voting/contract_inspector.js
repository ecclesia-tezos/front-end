import { TezosNodeReader } from 'conseiljs';
import axios from 'axios';

export default class ContractInspector {

  async getInitialState(contractAddress) {
    let contract = await this.getContract(contractAddress);

    return {
      modulus: this.parseModulus(contract)
    }
  }

  async getTimestamps(contractAddress) {
    let contract = await this.getContract(contractAddress);

    const timestampMapRef = this.parseTimestampMapRef(contract);

    let timestampResponse = await axios.get(
      `${process.env.REACT_APP_TZSTATS_URL}/explorer/bigmap/${timestampMapRef}/values?limit=${process.env.REACT_APP_TZSTATS_REQUEST_LIMIT}`
    );

    return timestampResponse.data.map(entry => entry.value);
  }

  async getCredentials(contractAddress) {
    let contract = await this.getContract(contractAddress);

    const credentialsMapRef = this.parseCredentialsMapRef(contract);

    let credentialsResponse = await axios.get(
      `${process.env.REACT_APP_TZSTATS_URL}/explorer/bigmap/${credentialsMapRef}/values?limit=${process.env.REACT_APP_TZSTATS_REQUEST_LIMIT}`
    );

    const credentials = credentialsResponse.data.map(entry => entry.value).filter(entry => entry !== '');
    return credentials;
  }

  async getVotes(contractAddress) {
    let contract = await this.getContract(contractAddress);

    const voteMapRefs = this.parseVoteMapRefs(contract);
    let voteMaps = [];
    for (let voteMapRef of voteMapRefs) {
      let voteMap = await axios.get(
        `${process.env.REACT_APP_TZSTATS_URL}/explorer/bigmap/${voteMapRef}/values?limit=${process.env.REACT_APP_TZSTATS_REQUEST_LIMIT}`
      );
      voteMaps.push(voteMap);
    }
    voteMaps = voteMaps.map(voteMap => voteMap.data.map(datum => {
      return { key: datum.key, value: datum.value };
    }));

    const openingMapRef = this.parseOpeningMapRef(contract);
    let openingMap = await axios.get(
      `${process.env.REACT_APP_TZSTATS_URL}/explorer/bigmap/${openingMapRef}/values?limit=${process.env.REACT_APP_TZSTATS_REQUEST_LIMIT}`
    );
    openingMap = openingMap.data.map(datum => {
      return {key: datum.key, value: datum.value }
    });

    let keys = openingMap.map(pair => pair.key);

    let votes = [];
    for (let key of keys) {
      let voteCommitmentPieces = voteMaps.map(
        voteMap => voteMap.find(pair => pair.key === key).value
      );
      let voteCommitment = voteCommitmentPieces.join('');
      let voteOpening = openingMap.find(opening => opening.key === key).value;
      votes.push({ voteCommitment: voteCommitment, voteOpening: voteOpening});
    }
    return votes;
  }

  parseModulus(contract) {
    return contract.script.storage.args[0].args[0].args[1].string;
  }

  parseCredentialsMapRef(contract) {
    return contract.script.storage.args[0].args[0].args[0].int;
  }

  parseVoteMapRefs(contract) {
    return [
      contract.script.storage.args[1].args[0].args[0].int,
      contract.script.storage.args[1].args[0].args[1].int,
      contract.script.storage.args[1].args[1].args[0].int,
    ];
  }

  parseOpeningMapRef(contract) {
    return contract.script.storage.args[1].args[1].args[1].int;
  }

  parseTimestampMapRef(contract) {
    return contract.script.storage.args[0].args[1].args[1].int;
  }

  async getContract(contractAddress) {
    const TEZOS_NODE = process.env.REACT_APP_TEZOS_NODE_URL;
    try {
      let blockHead = await TezosNodeReader.getBlockHead(TEZOS_NODE);
      let contract = await TezosNodeReader.getAccountForBlock(
        TEZOS_NODE, blockHead.hash, contractAddress
      );
      return contract;
    } catch(error) {
      return await this.getContract(contractAddress);
    }
  }
}
