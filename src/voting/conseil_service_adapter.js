import axios from 'axios';

export default class ConseilServiceAdapter {
  constructor() {
    this.axios = axios.create({
      baseURL: process.env.REACT_APP_CONSEIL_SERVICE_URL,
      timeout: 0
    });
  }

  async addCredential(credential, token, contractAddress) {
    try {
      let response = await this.axios.post('/credentials', {
        credential: credential,
        token: token,
        contractAddress: contractAddress
      });
      return response.data;
    } catch(error) {
      return await this.addCredential(credential, token, contractAddress);
    }
  }

  async vote(voteCommitment, index, token, contractAddress) {
    try {
      let response = await this.axios.post('/vote', {
        voteCommitment: voteCommitment,
        index: index,
        token: token,
        contractAddress: contractAddress
      });
      console.log(response);
      return response.data;
    } catch(error) {
      return await this.vote(voteCommitment, index, token, contractAddress);
    }
  }

  async openVote(voteKey, token, contractAddress) {
    try {
      let response = await this.axios.post('/open', {
        voteKey: voteKey,
        token: token,
        contractAddress: contractAddress
      });
      console.log(response);
      return response.data;
    } catch(error) {
      return await this.openVote(voteKey, token, contractAddress);
    }
  }
}
